# Spring_boot_thymeleaf_layout

##1. 설명
Thymeleaf Layout 기능 샘플

##2. Project 생성 환경
1. Mac OSX
2. IntelliJ IDEA
3. JDK 1.8

##3. 구성
1. Spring Boot
2. Thymeleaf
3. Thymeleaf Layout Dialect

##4. 실행방법
1. `git clone https://github.com/wonzopein/spring_boot_thymeleaf_layout_dialect.git`
2. `mvn spring-boot:run`
3. 브라우저에서 `http://localhost:8080` 접속