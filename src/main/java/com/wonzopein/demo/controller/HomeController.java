package com.wonzopein.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The type Home controller.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    String index(){
        return "index";
    }


    @RequestMapping("/main")
    String main(){
        return "main";
    }

    @RequestMapping("/main1")
    String main1(){
        return "main1";
    }

}
